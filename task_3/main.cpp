#include <iostream>
#include "adapter.h"


int main()
{
    Figure f(120, 35);
    Text text("Hello", 12, 45, true);
    Figure *tv = new TextView(text);

    tv->flip();
    tv->scale(12);
    tv->rotate(30);

    return 0;
}
