#include <iostream>
#include "strategy.h"


int main()
{
    TextEditor te("qwertyuiasdfghjk");
    DivideByScreenWidth *dsw = new DivideByScreenWidth;
    DivideByUserWidth *duw = new DivideByUserWidth;
    DivideBySentence *ds = new DivideBySentence;

    te.PrintText();
    te.setStrategy(dsw);
    te.useStrategy();
    te.setStrategy(duw);
    te.useStrategy();
    te.setStrategy(ds);
    te.useStrategy();

    return 0;
}
