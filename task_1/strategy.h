#ifndef STRATEGY_H
#define STRATEGY_H

#include <iostream>
#include <fstream>


class DivideStrategy
{
public:
    virtual void Divide(const std::string &text, size_t width) = 0;
};


class DivideByScreenWidth : public DivideStrategy
{
public:
    void Divide(const std::string& text, size_t width) override
    {
        std::cout << "DivideByScreenWidth Strategy by screen width = " << width << std::endl;
    }
};


class DivideByUserWidth : public DivideStrategy
{
public:
    void Divide(const std::string& text, size_t width) override
    {
        std::cout << "DivideByUserWidth Strategy by user width = " << width << std::endl;
    }
};


class DivideBySentence : public DivideStrategy
{
public:
    void Divide(const std::string& text, size_t width) override
    {
        std::cout << "DivideBySentence Strategy" << std::endl;
    }
};


class Data
{
public:
    virtual ~Data() {}
    virtual void useStrategy() = 0;
    virtual void setStrategy(DivideStrategy*) = 0;
};


class TextEditor : public Data
{
private:
    size_t editorWidth = 12;
    std::string all_text;
    DivideStrategy *divideStrategy;

public:
    TextEditor() {};

    TextEditor(const std::string &text) : all_text(text) {};

    TextEditor(std::ifstream& file)
    {
        std::string line;

        if(file.is_open())
        {
            while(getline(file, line))
            {
                all_text += line;
            }
        }
    }

    void PrintText() const
    {
        std::cout << all_text << std::endl;
    }

    void useStrategy() override
    {
        divideStrategy->Divide(all_text, editorWidth);
    }

    void setStrategy(DivideStrategy* strategy) override
    {
        divideStrategy = strategy;
    }

    virtual ~TextEditor()
    {
        delete divideStrategy;
    }
};

#endif // STRATEGY_H
