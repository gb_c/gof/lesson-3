#ifndef ADAPTER_H
#define ADAPTER_H

#include <iostream>


class IShape
{
public:
    virtual void scale(double scale_percentage) = 0;    // Масштабируем фигуру
    virtual void rotate(double angle) = 0;              // Поворачиваем фигуру
    virtual void flip() = 0;                            // Отражаем фигуру
};


class Figure : public IShape
{
private:
    int width;
    int height;
    double angle;
    bool isFlipped;

public:
    Figure(int width, int height)
        : width(width)
        , height(height)
        , angle(0.0)
        , isFlipped(false)
    {}

    Figure() {}

    void scale(double scale_percentage) override
    {
        width *= scale_percentage;
        height *= scale_percentage;
    }

    void rotate(double angle) override
    {
        angle += angle;
    }

    void flip() override
    {
        isFlipped = !isFlipped;
    }
};


class IText
{
    virtual void newSize(int size) = 0;     // Изменяем размер шрифта текста
    virtual void rotate(double angle) = 0;  // Поворачиваем текст
    virtual void reverse() = 0;             // Изменяем направление текста
};


class Text : public IText
{
private:
    int size;
    double angle;
    bool isReversed;
    std::string text;

public:
    Text(const std::string text, int size, double angle, bool isReversed)
        : text(text)
        , size(size)
        , angle(angle)
        , isReversed(isReversed)
    {}

    void newSize(int newSize) override
    {
        size = newSize;
    }

    void rotate(double newAngle) override
    {
        angle = newAngle;
    }

    void reverse() override
    {
        for(int i = 0; i < text.size()/2; ++i)
        {
            char c = text[i];
            text[i] = text[text.size() - 1 - i];
            text[text.size() - 1 - i] = c;
        }
    }
};


class TextView : public Figure
{
private:
    Text m_text;

public:
    TextView(const Text &text) : m_text(text)
    {}

    void scale(double scale_percentage) override
    {
        m_text.newSize(static_cast<int>(scale_percentage));
        std::cout << "TextView scale to " << scale_percentage << std::endl;
    }

    void rotate(double angle) override
    {
        std::cout << "TextView rotate to " << angle << std::endl;
        m_text.rotate(angle);
    }

    void flip() override
    {
        m_text.reverse();
        std::cout << "TextView flip" << std::endl;
    }
};

#endif // ADAPTER_H
