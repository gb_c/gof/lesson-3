#ifndef ITERATOR_ARRAY_H
#define ITERATOR_ARRAY_H

#include<iostream>
#include<array>


template <class T>
class MyArray
{
private:
    size_t indexBack = 0;
    std::array<T, 6> arr;

public:
    void push_back(T num)
    {
        if(indexBack < arr.size())
            arr.at(indexBack++) = num;
    }

    void clear()
    {
        for(int i = 0; i < indexBack; ++i)
            arr.at(i) = 0;

        indexBack = 0;
    }

    class Iterator
    {
    private:
        std::array<T, 6>& refArr;
        int curIndex;
        T *ptr;

    public:
        Iterator(std::array<T, 6>& ref) : refArr(ref), curIndex(-1), ptr(nullptr)
        {
            ++(*this);
        }

        void reset()
        {
            curIndex = -1;
            ptr = nullptr;
        }

        Iterator &operator++()
        {
            curIndex++;
            if(curIndex == -1)
                curIndex = 0;

            ptr = &refArr[curIndex];

            if(curIndex == refArr.size())
            {
                curIndex = -1;
                ptr = nullptr;
            }
            return *this;
        }

        Iterator &operator--()
        {
            if(curIndex == -1)
                curIndex = refArr.size() - 1;

            ptr = &refArr[curIndex];

            if(curIndex == -1)
            {
                ptr = nullptr;
            }
            return *this;
        }

        T &operator*()
        {
            return refArr.at(curIndex);
        }

        bool operator==(Iterator it)
        {
            if(curIndex == it.curIndex &&
               ptr == it.ptr &&
               refArr == it.refArr)
            {
                return true;
            }

            return false;
        }

        bool operator!=(Iterator it)
        {
            return !(*this == it);
        }
    };


    class Reverse_Iterator
    {
    private:
        std::array<T, 6>& refArr;
        int curIndex;
        T *ptr;

    public:
        Reverse_Iterator(std::array<T, 6>& ref) : refArr(ref), curIndex(-1), ptr(nullptr)
        {
            ++(*this);
        }

        void reset()
        {
            curIndex = -1;
            ptr = nullptr;
        }

        Reverse_Iterator &operator--()
        {
            curIndex++;
            if(curIndex == -1)
                curIndex = 0;

            ptr = &refArr[curIndex];

            if(curIndex == refArr.size())
            {
                curIndex = -1;
                ptr = nullptr;
            }
            return *this;
        }

        Reverse_Iterator &operator++()
        {
            curIndex--;
            if(curIndex < -1)
                curIndex = refArr.size() - 1;

            ptr = &refArr[curIndex];

            if(curIndex == -1)
            {
                ptr = nullptr;
            }
            return *this;
        }

        T &operator*()
        {
            return refArr.at(curIndex);
        }

        bool operator==(Reverse_Iterator it)
        {
            if(curIndex == it.curIndex &&
               ptr == it.ptr &&
               refArr == it.refArr)
            {
                return true;
            }

            return false;
        }

        bool operator!=(Reverse_Iterator it)
        {
            return !(*this == it);
        }
    };


    Iterator begin()
    {
        Iterator it(arr);
        return it;
    }

    Iterator end()
    {
        Iterator it(arr);
        it.reset();
        return it;
    }

    Reverse_Iterator rbegin()
    {
        Reverse_Iterator it(arr);
        return it;
    }

    Reverse_Iterator rend()
    {
        Reverse_Iterator it(arr);
        it.reset();
        return it;
    }
};

#endif // ITERATOR_ARRAY_H
