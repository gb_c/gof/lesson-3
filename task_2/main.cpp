#include <iostream>
#include "iterator_vector.h"
#include "iterator_array.h"
#include "iterator_list.h"
#include "iterator_deque.h"


// Доп задание только через шаблоны знаю как сделать


class Employer
{
private:
    std::string name;
    std::string description;
    bool isMarried;
    int age;

public:
    Employer() {}
    Employer(const std::string &name, const std::string &description, bool isMarried, int age)
        : name(name)
        , description(description)
        , isMarried(isMarried)
        , age(age)
    {}

    const std::string &getName() const {
        return name;
    }

    const std::string &getDescription() const {
        return description;
    }

    bool getMarried() const {
        return isMarried;
    }

    int getAge() const {
        return age;
    }

    bool operator<(Employer &other){
        return name < other.name;
    }

    friend std::ostream& operator<<(std::ostream& os, const Employer &e);

    friend bool operator==(const Employer& e1, const Employer& e2);
};


std::ostream& operator<<(std::ostream &os, const Employer &e)
{
    os << e.getName() << " " << e.getDescription() << " "
       << e.getMarried() << " " << e.getAge();
    return os;
}


bool operator==(const Employer& e1, const Employer& e2)
{
    return e1.getAge() == e2.getAge() && e1.getName() == e2.getName() &&
           e1.getMarried() == e2.getMarried() && e1.getDescription() == e2.getDescription();
}


int main()
{
    // Vector
    MyVector<double> vec;

    vec.push_back(std::rand() % 100);
    vec.push_back(std::rand() % 100);
    vec.push_back(std::rand() % 100);
    vec.push_back(std::rand() % 100);
    vec.push_back(std::rand() % 100);
    vec.push_back(std::rand() % 100);

    MyVector<double>::Iterator itVec1 = vec.begin();
    std::cout << "My vector: " << std::endl;
    for(; itVec1 != vec.end(); ++itVec1)
    {
        std::cout << *itVec1 << " ";
    }
    std::cout << std::endl;

    std::cout << "My vector reverse: " << std::endl;
    MyVector<double>::Reverse_Iterator itVec2 = vec.rbegin();
    for(; itVec2 != vec.rend(); ++itVec2)
    {
        std::cout << *itVec2 << " ";
    }
    std::cout << std::endl << std::endl;


    // Array
    MyArray<double> arr;

    arr.push_back(std::rand() % 100);
    arr.push_back(std::rand() % 100);
    arr.push_back(std::rand() % 100);
    arr.push_back(std::rand() % 100);
    arr.push_back(std::rand() % 100);
    arr.push_back(std::rand() % 100);

    MyArray<double>::Iterator itArr1 = arr.begin();
    std::cout << "My array: " << std::endl;
    for(; itArr1 != arr.end(); ++itArr1)
    {
        std::cout << *itArr1 << " ";
    }
    std::cout << std::endl;

    std::cout << "My array reverse: " << std::endl;
    MyArray<double>::Reverse_Iterator itArr2 = arr.rbegin();
    for(; itArr2 != arr.rend(); ++itArr2)
    {
        std::cout << *itArr2 << " ";
    }
    std::cout << std::endl << std::endl;


    // List
    MyList<int> list;

    list.push_back(std::rand() % 100);
    list.push_back(std::rand() % 100);
    list.push_back(std::rand() % 100);
    list.push_back(std::rand() % 100);
    list.push_back(std::rand() % 100);
    list.push_back(std::rand() % 100);

    MyList<int>::Iterator itList1 = list.begin();
    std::cout << "My list: " << std::endl;
    for(; itList1 != list.end(); ++itList1)
    {
        std::cout << *itList1 << " ";
    }
    std::cout << std::endl;

    std::cout << "My list reverse: " << std::endl;
    MyList<int>::Reverse_Iterator itList2 = list.rbegin();
    for(; itList2 != list.rend(); ++itList2)
    {
        std::cout << *itList2 << " ";
    }
    std::cout << std::endl << std::endl;


    // Deque
    MyDeque<Employer> deq;

    deq.push_back(Employer("Vasya", "Pupkin", true, 35));
    deq.push_back(Employer("Ivan", "Popov", false, 29));
    deq.push_back(Employer("Max", "Ivanov", false, 16));
    deq.push_back(Employer("Petr", "Petrov", true, 43));

    MyDeque<Employer>::Iterator itDeq1 = deq.begin();
    std::cout << "My deque: " << std::endl;
    for(; itDeq1 != deq.end(); ++itDeq1)
    {
        std::cout << std::boolalpha << *itDeq1 << std::endl;
    }
    std::cout << std::endl;

    std::cout << "My deque reverse: " << std::endl;
    MyDeque<Employer>::Reverse_Iterator itDeq2 = deq.rbegin();
    for(; itDeq2 != deq.rend(); ++itDeq2)
    {
        std::cout << std::boolalpha << *itDeq2 << std::endl;
    }
    std::cout << std::endl;

    return 0;
}
